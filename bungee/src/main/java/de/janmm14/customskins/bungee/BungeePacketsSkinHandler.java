package de.janmm14.customskins.bungee;

import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.protocol.DefinedPacket;
import net.md_5.bungee.protocol.packet.PlayerListItem;

import de.janmm14.customskins.core.wrapper.SkinHandler;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.spawl.bungeepackets.event.PacketEvent;
import lombok.NonNull;

public class BungeePacketsSkinHandler implements SkinHandler, Listener {

	private CustomSkinsBungee plugin;
	/**
	 * shownTo, shown -> display name
	 */
	@NonNull
	private final Table<UUID, UUID, String> displayNameCache = HashBasedTable.create();
	@NonNull
	private final ReentrantLock displayNameCacheLock = new ReentrantLock();

	public BungeePacketsSkinHandler(CustomSkinsBungee plugin) {
		this.plugin = plugin;
		plugin.getProxy().getPluginManager().registerListener(plugin, this);
	}

	@EventHandler
	public void onPacketSent(PacketEvent event) {
		DefinedPacket definedPacket = event.getPacket();
		if (!(definedPacket instanceof PlayerListItem)) {
			return;
		}
		if (!(event.getReciever() instanceof ProxiedPlayer)) { //indicates the sender is the server behind
			return;
		}
		PlayerListItem packet = (PlayerListItem) definedPacket;
		if (packet.getAction() == PlayerListItem.Action.UPDATE_DISPLAY_NAME) {
			PlayerListItem.Item[] items = packet.getItems();
			int itemAmount = items.length;
			if (itemAmount == 0) {
				return;
			}
			displayNameCacheLock.lock();
			try {
				for (PlayerListItem.Item item : items) {
					if (item.getDisplayName() == null || item.getDisplayName().equals(item.getUsername())) {
						displayNameCache.remove(event.getPacket(), item.getUuid());

					} else {
						displayNameCache.put(event.getPlayer().getUniqueId(), item.getUuid(), item.getDisplayName());

					}
				}
			} finally {
				displayNameCacheLock.unlock();
			}
			return;
		}
		if (packet.getAction() != PlayerListItem.Action.ADD_PLAYER) { // starting here with ADD_PLAYER action handling
			return;
		}
		PlayerListItem.Item[] items = packet.getItems();
		int itemAmount = items.length;
		if (itemAmount == 0) {
			return;
		}
		try {
			int i = 0;
			for (PlayerListItem.Item item : items) {
				final String[][] profileProperties = item.getProperties();
				System.out.println("PlayerListItem #" + i++ + ": ");
				System.out.println(Arrays.deepToString(profileProperties)); //TODO get output and start then

				//TODO implement for bungee
				// map for property replacements
				/*final String[][] newProfileProperties = Arrays.copyOf(profileProperties, profileProperties.length);

				boolean textureDataExists = false;
				// iterate through properties safely (currently only textures property is there)
				for (Map.Entry<String, WrappedSignedProperty> entry : profileProperties.entries()) {
					if (entry == null) {
						continue;
					}
					if (entry.getKey().equalsIgnoreCase("textures")) {
						textureDataExists = true;
						String skinName = plugin.getData().getSkinNameIdByUuid(profile.getUUID());
						// if skin is changed
						if (skinName != null && !skinName.isEmpty()) {
							Skin skin = plugin.getData().getCachedSkin(skinName);
							//ensure the skin exists
							if (skin != null) {
								//put value in replacement map
								newProfileProperties.put(entry.getKey(), new WrappedSignedProperty(
									entry.getValue().getName(),
									skin.getData(),
									skin.getSignature()));
								continue;
							}
						}
					}
					// if skin is not changed, put old values in map
					newProfileProperties.put(entry.getKey(), entry.getValue());
				}
				//if no texture data is there, create some and apply changed skin directly if happend
				if (!textureDataExists) {
					String skinName = plugin.getData().getSkinNameIdByUuid(profile.getUUID());
					if (skinName != null && !skinName.isEmpty()) {
						Skin skin = plugin.getData().getCachedSkin(skinName);
						if (skin != null) {
							newProfileProperties.put("textures", new WrappedSignedProperty("textures", skin.getData(), skin.getSignature()));
						}
					}
				}
				// apply changes from our map
				profileProperties.clear();
				profileProperties.putAll(newProfileProperties);

				// ensure we don't change the display name of the specific user by caching it
				WrappedChatComponent displayName = playerInfoData.getDisplayName();
				if (displayName != null) {
					displayNameCacheLock.lock();
					try {
						displayNameCache.put(event.getPlayer().getUniqueId(), profile.getUUID(), displayName);
					} finally {
						displayNameCacheLock.unlock();
					}
				} /*else { //not needed anymore as ProtocolLib got patched, see comment in updatePlayerSkin(Player)
					displayName = WrappedChatComponent.fromText(profile.getName()); //protocollib seems to require this optinal field
				}*/
				// add player info data to new data list
				/*newData.add(new PlayerInfoData(profile, playerInfoData.getPing(), playerInfoData.getGameMode(), displayName));*/
			}
			//packet.setData(newData);
		} catch (Throwable t) {
			StringBuilder sb = new StringBuilder(items.length * 8); //just assume the average name length is 8
			boolean first = true;
			for (PlayerListItem.Item item : items) {
				if (first)
					first = false;
				else
					sb.append(", ");
				sb.append(item.getUsername());
			}
			plugin.getLogger().severe("Could not transform player list packet for player " + event.getPlayer().getName() +
				" about these players: " + sb);
			t.printStackTrace();
		}
		//event.setPacket(packet.getHandle());
	}

	@Override
	public void updatePlayerSkin(@NonNull UUID uuid) {
		//TODO: implement
	}
}
