package de.janmm14.customskins.bungee.wrapperimpl.config;

import java.util.HashSet;
import java.util.Set;
import javax.annotation.Nullable;

import net.md_5.bungee.config.Configuration;

import de.janmm14.customskins.core.wrapper.config.ConfigurationSectionWrapper;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor
public class BungeeConfigurationSectionWrapper implements ConfigurationSectionWrapper {

	protected Configuration cfg; //sections also have Configuration class

	@Override
	@Nullable
	public String getString(@NonNull String path) {
		return cfg.getString(path);
	}

	@Override
	@Nullable
	public String getString(@NonNull String path, @Nullable String def) {
		return cfg.getString(path, def);
	}

	@Override
	public int getInt(@NonNull String path) {
		return cfg.getInt(path);
	}

	@Override
	public long getLong(@NonNull String path) {
		return cfg.getLong(path);
	}

	@Override
	public boolean getBoolean(@NonNull String path) {
		return cfg.getBoolean(path);
	}

	@Override
	public void set(@NonNull String path, @Nullable Object value) {
		cfg.set(path, value);
	}

	@Override
	public void addDefault(@NonNull String path, @NonNull Object value) {
		if (cfg.get(path) == null) {
			cfg.set(path, value);
		}
	}

	@Override
	@NonNull
	public Set<String> getKeys(boolean deep) {
		if (deep) {
			throw new UnsupportedOperationException("Bungee configuration api does not support deep keys getting");
		}
		return new HashSet<>(cfg.getKeys());
	}

	@Override
	@NonNull
	public ConfigurationSectionWrapper getConfigurationSection(@NonNull String path) {
		return new BungeeConfigurationSectionWrapper(cfg.getSection(path));
	}

	@Override
	public boolean isValid() {
		return cfg != null;
	}

	@Override
	@Nullable
	public Configuration getWrapped() {
		return cfg;
	}
}
