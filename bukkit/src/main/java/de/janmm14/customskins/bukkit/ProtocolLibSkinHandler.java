package de.janmm14.customskins.bukkit;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import de.janmm14.customskins.core.data.Skin;
import de.janmm14.customskins.core.wrapper.SkinHandler;

import com.comphenix.packetwrapper.WrapperPlayServerPlayerInfo;
import com.comphenix.packetwrapper.WrapperPlayServerRespawn;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.injector.GamePhase;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedSignedProperty;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table;
import lombok.NonNull;

public class ProtocolLibSkinHandler extends PacketAdapter implements Listener, SkinHandler {

	@NonNull
	private final CustomSkins plugin;

	/**
	 * shownTo, shown -> display name
	 */
	@NonNull
	private final Table<UUID, UUID, WrappedChatComponent> displayNameCache = HashBasedTable.create();
	@NonNull
	private final ReentrantLock displayNameCacheLock = new ReentrantLock();

	public ProtocolLibSkinHandler(@NonNull CustomSkins plugin) {
		super(new AdapterParameteters()
			.plugin(plugin)
			.gamePhase(GamePhase.BOTH)
			.serverSide()
			.optionAsync()
			.listenerPriority(ListenerPriority.HIGHEST)
			.types(PacketType.Play.Server.PLAYER_INFO));
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@Override
	public void onPacketSending(PacketEvent event) {
		if (event.getPacketType() != PacketType.Play.Server.PLAYER_INFO) {
			return;
		}
		final WrapperPlayServerPlayerInfo packet = new WrapperPlayServerPlayerInfo(event.getPacket());
		final List<PlayerInfoData> packetData = packet.getData();
		if (packet.getAction() == EnumWrappers.PlayerInfoAction.UPDATE_DISPLAY_NAME) {
			if (packetData == null) {
				plugin.getLogger().warning("packetData is null, could not change any skins");
				return;
			}
			displayNameCacheLock.lock();
			try {
				for (PlayerInfoData pd : packetData) {
					if (pd.getDisplayName() == null) {
						displayNameCache.remove(event.getPacket(), pd.getProfile().getUUID());
					} else {
						displayNameCache.put(event.getPlayer().getUniqueId(), pd.getProfile().getUUID(), pd.getDisplayName());
					}
				}
			} finally {
				displayNameCacheLock.unlock();
			}
			return;
		}
		if (packet.getAction() != EnumWrappers.PlayerInfoAction.ADD_PLAYER) { // starting here with ADD_PLAYER action handling
			return;
		}
		if (packetData == null) {
			plugin.getLogger().warning("packetData is null, could not change any skins");
			return;
		}
		try {
			List<PlayerInfoData> newData = new ArrayList<>(packetData.size());
			//Iterate through all player list items sent in that packet
			for (PlayerInfoData playerInfoData : packetData) {
				WrappedGameProfile profile = playerInfoData.getProfile();
				if (profile == null) {
					plugin.getLogger().warning("profile is null, could not change any skins");
					return;
				}

				//this is no copy, so changes are reflected directly in WrappedGameProfile
				final Multimap<String, WrappedSignedProperty> profileProperties = profile.getProperties();

				// map for property replacements
				final Multimap<String, WrappedSignedProperty> newProfileProperties = createListMultimap(profileProperties.size(), 1);

				boolean textureDataExists = false;
				// iterate through properties safely (currently only textures property is there)
				for (Map.Entry<String, WrappedSignedProperty> entry : profileProperties.entries()) {
					if (entry == null) {
						continue;
					}
					if (entry.getKey().equalsIgnoreCase("textures")) {
						textureDataExists = true;
						String skinName = plugin.getData().getSkinNameIdByUuid(profile.getUUID());
						// if skin is changed
						if (skinName != null && !skinName.isEmpty()) {
							Skin skin = plugin.getData().getCachedSkin(skinName);
							//ensure the skin exists
							if (skin != null) {
								//put value in replacement map
								newProfileProperties.put(entry.getKey(), new WrappedSignedProperty(
									entry.getValue().getName(),
									skin.getData(),
									skin.getSignature()));
								continue;
							}
						}
					}
					// if skin is not changed, put old values in map
					newProfileProperties.put(entry.getKey(), entry.getValue());
				}
				//if no texture data is there, create some and apply changed skin directly if happend
				if (!textureDataExists) {
					String skinName = plugin.getData().getSkinNameIdByUuid(profile.getUUID());
					if (skinName != null && !skinName.isEmpty()) {
						Skin skin = plugin.getData().getCachedSkin(skinName);
						if (skin != null) {
							newProfileProperties.put("textures", new WrappedSignedProperty("textures", skin.getData(), skin.getSignature()));
						}
					}
				}
				// apply changes from our map
				profileProperties.clear();
				profileProperties.putAll(newProfileProperties);

				// ensure we don't change the display name of the specific user by caching it
				WrappedChatComponent displayName = playerInfoData.getDisplayName();
				if (displayName != null) {
					displayNameCacheLock.lock();
					try {
						displayNameCache.put(event.getPlayer().getUniqueId(), profile.getUUID(), displayName);
					} finally {
						displayNameCacheLock.unlock();
					}
				} /*else { //not needed anymore as ProtocolLib got patched, see comment in updatePlayerSkin(Player)
					displayName = WrappedChatComponent.fromText(profile.getName()); //protocollib seems to require this optinal field
				}*/
				// add player info data to new data list
				newData.add(new PlayerInfoData(profile, playerInfoData.getPing(), playerInfoData.getGameMode(), displayName));
			}
			packet.setData(newData);
		} catch (Throwable t) {
			StringBuilder sb = new StringBuilder(packetData.size() * 8); //just assume the average name length is 8
			boolean first = true;
			for (PlayerInfoData playerInfoData : packetData) {
				if (first)
					first = false;
				else
					sb.append(", ");
				sb.append(playerInfoData.getProfile().getName());
			}
			plugin.getLogger().severe("Could not transform player list packet for player " + event.getPlayer().getName() +
				" about these players: " + sb);
			t.printStackTrace();
		}
		event.setPacket(packet.getHandle());
	}

	@NonNull
	private <K, V> ListMultimap<K, V> createListMultimap(int expectedKeys, int expectedValuesPerKey) {
		return ArrayListMultimap.create(expectedKeys, expectedValuesPerKey);
	}

	@SuppressWarnings({"Convert2streamapi", "UnusedCatchParameter"})
	public void updatePlayerSkin(@NonNull final Player target) {
		//TODO look whether its needed to remove and add player to the list or just hide and show it through bukkit
		if (!updateTablist(target)) {
			return;
		}

		Runnable hideShowTask = () -> {
			Collection<? extends Player> seeablePlayers = new HashSet<>(plugin.getServer().getOnlinePlayers());
			//noinspection SuspiciousMethodCalls
			seeablePlayers.removeAll(target.spigot().getHiddenPlayers());
			for (Player plr : seeablePlayers) {
				plr.hidePlayer(target);
			}

			// Adapted fix by claytonbd - https://bitbucket.org/claytonbd/custom-skins/commits/c7e3468c4d084753dddd098ad57b0e875b70d49f#Lsrc/main/java/de/janmm14/customskins/listener/PlayerInfoPacketListener.javaT297
			Location loc = target.getLocation().clone();
			WrapperPlayServerRespawn packet = new WrapperPlayServerRespawn();
			packet.setDifficulty(EnumWrappers.Difficulty.valueOf(target.getWorld().getDifficulty().toString()));
			packet.setDimension(target.getWorld().getEnvironment().getId());
			packet.setGamemode(EnumWrappers.NativeGameMode.fromBukkit(target.getGameMode()));
			packet.setLevelType(target.getWorld().getWorldType());
			packet.sendPacket(target);

			plugin.getServer().getScheduler().runTask(plugin, () -> {
				// show the players again with a little delay to prevent issues
				for (Player plr : seeablePlayers) {
					plr.showPlayer(target);
				}
				//set location again for safety after respawning the client
				target.teleport(loc);

				//set flying again for safety after respawning the client
				if (target.getAllowFlight()) {
					boolean flying = target.isFlying();
					target.setFlying(false);
					target.setAllowFlight(false);
					target.setAllowFlight(true);
					target.setFlying(flying);
					target.teleport(loc);
				}
			});
		};
		try {
			hideShowTask.run();
		} catch (IllegalStateException | ConcurrentModificationException ex) {
			//in case spigot does not like async, just do it sync!
			plugin.getServer().getScheduler().runTask(plugin, hideShowTask);
		}
	}

	@SuppressWarnings("Convert2streamapi")
	private boolean updateTablist(@NonNull Player target) {
		// first: remove player's of tablist so we can set skin data again
		try {
			WrapperPlayServerPlayerInfo packet = new WrapperPlayServerPlayerInfo();
			packet.setAction(EnumWrappers.PlayerInfoAction.REMOVE_PLAYER);
			WrappedGameProfile profile = new WrappedGameProfile(target.getUniqueId(), null);
			packet.setData(Lists.newArrayList(new PlayerInfoData(profile, 0, EnumWrappers.NativeGameMode.SURVIVAL, null)));

			for (Player plr : plugin.getServer().getOnlinePlayers()) {
				if (plr.canSee(target)) {
					packet.sendPacket(plr);
				}
			}
		} catch (Throwable t) {
			plugin.getLogger().severe("Could not update player skin for player " + target.getName() + ':' +
				" Sending 'remove old skin' failed! Stopping update. Error:");
			t.printStackTrace();
			return false;
		}
		// after that: create new playerlistpacket containing all the data of the player
		try {
			WrapperPlayServerPlayerInfo basePacket = new WrapperPlayServerPlayerInfo();
			basePacket.setAction(EnumWrappers.PlayerInfoAction.ADD_PLAYER);
			WrappedGameProfile profile = new WrappedGameProfile(target.getUniqueId(), target.getName());
			final EnumWrappers.NativeGameMode nativeGameMode = EnumWrappers.NativeGameMode.fromBukkit(target.getGameMode());

			int ping = getPing(target);
			// send to all players
			for (Player plr : plugin.getServer().getOnlinePlayers()) {
				// ... except the ones who are hidden from the player
				if (plr.canSee(target)) {
					WrappedChatComponent displayName = null;

					// look whether the display name was changed
					displayNameCacheLock.lock();
					try {
						displayName = displayNameCache.get(plr, target);
					} finally {
						displayNameCacheLock.unlock();
					}

					// previously we had to set the display name as protocollib did not like it if we leave it out (wiki.vg says the packet allowes leaving out)
					/*if (displayName == null) {
						displayName = WrappedChatComponent.fromText(target.getName());
					}*/

					//create and send the packet
					WrapperPlayServerPlayerInfo packet = new WrapperPlayServerPlayerInfo(basePacket.getHandle());
					packet.setData(Collections.singletonList(new PlayerInfoData(profile, ping, nativeGameMode, displayName)));
					packet.sendPacket(plr); //invokes our own packet handler (this class) as this does not set any skin-related data
				}
			}
		} catch (Throwable t) {
			plugin.getLogger().severe("Could not update player skin for player " + target.getName() + ": Sending new skin data failed!");
			t.printStackTrace();
		}
		return true;
	}

	/**
	 * Get the current ping of a player
	 *
	 * @param target the player
	 * @return the ping or 0 if an error occurred
	 */
	private int getPing(@NonNull Player target) {
		int ping;
		Class<?> craftPlayer = target.getClass();
		try {
			Method getHandle = craftPlayer.getMethod("getHandle", (Class[]) null);
			Object entityPlayer = getHandle.invoke(target);
			Field pingField = entityPlayer.getClass().getField("ping");
			ping = (int) pingField.get(entityPlayer);
		} catch (ReflectiveOperationException e) {
			plugin.getLogger().warning("Could not get ping of player " + target.getName() + ". Error:");
			e.printStackTrace();
			ping = 0;
		}
		return ping;
	}

	private void removeCachedDisplayNames(@NonNull UUID uuid) {
		displayNameCacheLock.lock();
		try {
			Map<UUID, WrappedChatComponent> row = new HashMap<>(displayNameCache.row(uuid));
			for (UUID shown : row.keySet()) {
				displayNameCache.remove(uuid, shown);
			}
			Map<UUID, WrappedChatComponent> column = new HashMap<>(displayNameCache.column(uuid));
			for (UUID shownTo : column.keySet()) {
				displayNameCache.remove(shownTo, uuid);
			}
		} finally {
			displayNameCacheLock.unlock();
		}
	}

	private void removeCachedDisplayNamesAsync(UUID uuid) {
		plugin.getServer().getScheduler()
			.runTask(plugin, () -> removeCachedDisplayNames(uuid));
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerQuit(PlayerQuitEvent event) {
		removeCachedDisplayNamesAsync(event.getPlayer().getUniqueId());
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerKick(PlayerKickEvent event) {
		removeCachedDisplayNamesAsync(event.getPlayer().getUniqueId());
	}

	@Override
	public void updatePlayerSkin(@NonNull UUID uuid) {
		Player target = plugin.getServer().getPlayer(uuid);
		if (target != null) {
			updatePlayerSkin(target);
		}
	}
}
