package de.janmm14.customskins.bukkit.wrapperimpl.config;

import java.util.Set;
import javax.annotation.Nullable;

import org.bukkit.configuration.ConfigurationSection;

import de.janmm14.customskins.core.wrapper.config.ConfigurationSectionWrapper;

import lombok.AllArgsConstructor;
import lombok.NonNull;

@AllArgsConstructor
public class BukkitConfigurationSectionWrapper implements ConfigurationSectionWrapper {

	protected ConfigurationSection cfg;

	@Override
	@Nullable
	public String getString(@NonNull String path) {
		return cfg.getString(path);
	}

	@Override
	@Nullable
	public String getString(@NonNull String path, @Nullable String def) {
		return cfg.getString(path, def);
	}

	@Override
	public int getInt(@NonNull String path) {
		return cfg.getInt(path);
	}

	@Override
	public long getLong(@NonNull String path) {
		return cfg.getLong(path);
	}

	@Override
	public boolean getBoolean(@NonNull String path) {
		return cfg.getBoolean(path);
	}

	@Override
	public void set(@NonNull String path, @Nullable Object value) {
		cfg.set(path, value);
	}

	@Override
	public void addDefault(@NonNull String path, @NonNull Object value) {
		cfg.addDefault(path, value);
	}

	@NonNull
	@Override
	public Set<String> getKeys(boolean deep) {
		return cfg.getKeys(deep);
	}

	@NonNull
	@Override
	public ConfigurationSectionWrapper getConfigurationSection(@NonNull String path) {
		return new BukkitConfigurationSectionWrapper(cfg.getConfigurationSection(path));
	}

	@Override
	public boolean isValid() {
		return cfg != null;
	}

	@Override
	@Nullable
	public ConfigurationSection getWrapped() {
		return cfg;
	}
}
