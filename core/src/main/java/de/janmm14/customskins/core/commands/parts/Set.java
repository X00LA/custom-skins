package de.janmm14.customskins.core.commands.parts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import de.janmm14.customskins.core.commands.CmdPart;
import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;
import de.janmm14.customskins.core.wrapper.PluginWrapper;

import com.google.common.collect.Lists;
import lombok.NonNull;

public class Set extends CmdPart {

	public static final BaseComponent[] USAGE = new ComponentBuilder("/customskins set <name> <skinname>").color(ChatColor.GOLD).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/customskins set "))
		.append(" - ").color(ChatColor.GRAY)
		.append("sets the skin of the player with the given name to the skin of the given playername or the downloaded skin").color(ChatColor.YELLOW).create();

	public Set(PluginWrapper plugin) {
		super(plugin, "customskins.set", "set");
	}

	@Override
	public void onCommand(@NonNull final CommandSenderWrapper sender, @NonNull final String[] args) {
		if (args.length < 2) {
			sendUsage(sender);
			return;
		}
		getPlugin().getSchedulerWrapper().asyncNow(() -> {
			CommandSenderWrapper target = getPlugin().getPlayer(args[0]);
			if (target == null) {
				sender.sendMessage("§cPlayer not found / not online. Setting skins of offline players is currently unsupported.");
				return;
			}
			String skinId = args[1].toLowerCase();
			if (getPlugin().getData().getCachedSkin(skinId) == null) {
				sender.sendMessage("§cSkin §6" + skinId + "§cnot found!");
			}
			getPlugin().getData().setUsedSkin(target.getUuid(), skinId);
			getPlugin().getSkinHandler().updatePlayerSkin(target.getUuid());
			sender.sendMessage("§aSkin of §6" + target.getName() + " §achanged!");

			/*if (sender.isPlayer()) { //TO DO allow disabling that message
				target.sendMessage("§aYour skin was changed by " + sender.getName());
			} else {
				target.sendMessage("§aYour skin was changed by the mysterious one sitting in front of my heard!");
			}*/
		});
	}

	@NonNull
	@Override
	public List<String> onTabComplete(@NonNull CommandSenderWrapper sender, @NonNull String[] args) {
		// TODO provide tab complete for skin names
		if (args.length != 1)
			return Collections.emptyList();

		String s = args[0].toLowerCase();
		List<String> l = new ArrayList<>();
		for (CommandSenderWrapper player : getPlugin().getOnlinePlayers()) {
			String name = player.getName();
			if (name.toLowerCase().startsWith(s)) {
				l.add(name);
			}
		}
		return l;
	}

	@Override
	public void sendUsage(@NonNull CommandSenderWrapper sender) {
		sender.sendMessage(USAGE);
	}
}
