package de.janmm14.customskins.core.commands.parts;

import java.io.File;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import de.janmm14.customskins.core.commands.CmdPart;
import de.janmm14.customskins.core.data.Account;
import de.janmm14.customskins.core.data.Proxy;
import de.janmm14.customskins.core.data.Skin;
import de.janmm14.customskins.core.util.Network;
import de.janmm14.customskins.core.util.Util;
import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;
import de.janmm14.customskins.core.wrapper.PluginWrapper;
import de.janmm14.minecraftchangeskin.api.SkinChangeParams;
import de.janmm14.minecraftchangeskin.api.SkinChanger;
import de.janmm14.minecraftchangeskin.api.SkinChangerResult;
import de.janmm14.minecraftchangeskin.api.SkinModel;

import com.google.common.collect.Lists;
import lombok.NonNull;

public class Prepare extends CmdPart {

	public static final BaseComponent[] USAGE = new ComponentBuilder("/customskins prepare <skinname> <url>").color(ChatColor.GOLD).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/customskins prepare "))
		.append(" - ").color(ChatColor.GRAY)
		.append("prepares the skin at the given url to be set").color(ChatColor.YELLOW).create();

	public Prepare(PluginWrapper plugin) {
		super(plugin, "customskins.prepare", "prepare", "p", "prep");
	}

	@Override
	public void onCommand(@NonNull final CommandSenderWrapper sender, @NonNull final String[] args) {
		// TODO better chat messages
		getPlugin().getSchedulerWrapper().asyncNow(() -> {
			if (args.length < 2) {
				sendUsage(sender);
			} else {
				try {
					if (!Util.isAlphanumeric(args[0])) {
						sender.sendMessage("§cError: The skin name has to be alphanumeric!");
						return;
					}
					/*if (!cs.hasPermission("customskins.prepareoverride") && CustomSkins.getPlugin().getData().getCachedSkin(args[0].toLowerCase()) != null) {
						//TODO implement
						//TODO remove any data if command failed
					}*/
					File file = new File(getPlugin().getDataFolder(), "cache" + File.separator + args[0].toLowerCase() + ".png");
					// TODO check and validate url syntax

					boolean success = Network.downloadSkin(args[1], file);
					// TODO remove success boolean from Network's method and throw any errors, catch these in an inner catch block
					if (!success) {
						sender.sendMessage("§cCould not download skin. Error unknown.");
						return;
					}
					sender.sendMessage("Skin " + args[0] + " downloaded.");


					sender.sendMessage("Uploading skin " + args[0].toLowerCase() + " to minecraft.net ...");
					Account acc_ = null;
					Proxy proxy_ = null;
					boolean found = false;
					//can't import due to cmdpart set
					java.util.Set<Account> accounts = getPlugin().getData().getAccounts();
					if (accounts.size() <= 0) {
						sender.sendMessage("§cError: No login info specified in config.");
						return;
					}
					for (Proxy prox : getPlugin().getData().getProxies()) {
						for (Account account : accounts) {
							if (!account.getUsed().get() && (prox.getLastUsedMillis(account.getUuid()) + (1000 * 71)) < System.currentTimeMillis()) {
								account.getUsed().set(true);
								proxy_ = prox;
								acc_ = account;
								found = true;
								break;
							}
						}
						if (found)
							break;
					}
					if (acc_ == null) {
						sender.sendMessage("§cUnable to continue because no proxy would be able to recieve the skin data of any player right now. Retry later by reusing the command.");
						return;
					}
					final Account acc = acc_;
					final Proxy proxy = proxy_;
					SkinChanger.changeSkin(
						SkinChangeParams.Builder.create()
							.email(acc.getEmail())
							.password(acc.getPassword())
							.image(file)
							.skinModel(SkinModel.STEVE).build(), // TODO allow change of skin model
						(success1, error) -> {
							if (success1 == SkinChangerResult.SECURITY_QUESTIONS) {
								getPlugin().getLogger().warning("---------------------------------------");
								getPlugin().getLogger().warning("Could not upload skin to minecraft.net");
								getPlugin().getLogger().warning("To solve this, you need to login to minecraft.net once with this ip and answer the security questions!");
								getPlugin().getLogger().warning("For your account security and safety I will not implement automatically answering of the security questions.");
								getPlugin().getLogger().warning("---------------------------------------");
								sender.sendMessage("§cCould not upload skin to minecraft.net, look at the console to see information on how to solve this issue.");
							}
							if (error != null) {
								error.printStackTrace();
								sender.sendMessage("§cError while uploading the skin to minecraft.net!");
								return;
							}
							if (success1 == SkinChangerResult.UNKNOWN_ERROR) {
								sender.sendMessage("§cAn unknown error occurred!");
								return;
							}
							sender.sendMessage("Skin " + args[0].toLowerCase() + " uploaded to minecraft.net!");
							final long accountCooldown = getPlugin().getData().getAccountCooldown();
							sender.sendMessage("Waiting " + accountCooldown + " seconds so mojang caches can update.");
							try {
								Thread.sleep(accountCooldown * 1000);
							} catch (InterruptedException e) {
								getPlugin().getLogger().warning("Could not wait the required amount of time for updating the skin.");
								e.printStackTrace();
							}
							sender.sendMessage("Retrieving skin's data...");
							Skin skin = Network.getSkin(acc.getUuid(), proxy, args[0].toLowerCase());
							if (skin == null) {
								sender.sendMessage("An error occurred while trying to recieve the skin's data.");
								return;
							}
							proxy.setLastUsedMillis(acc.getUuid(), System.currentTimeMillis());
							getPlugin().getData().setCachedSkin(skin, "user: " + sender.getName() + " url: " + args[1]);
							sender.sendMessage("Skin data loaded. Skin " + args[0].toLowerCase() + " is ready for usage with /customskins set(me)");

							if (accountCooldown != 0) {
								try {
									Thread.sleep(TimeUnit.SECONDS.toMillis(accountCooldown));
								} catch (InterruptedException ignored) {
								}
							}
							acc.getUsed().set(false);
						});
				} catch (Exception ex) {
					sender.sendMessage("§cCould not prepare skin. Error: " + ex.getMessage());
					getPlugin().getLogger().log(Level.WARNING, "Could not prepare skin from url " + args[1], ex);
				}
			}
		});
	}

	@NonNull
	@Override
	public List<String> onTabComplete(@NonNull CommandSenderWrapper sender, @NonNull String[] restArgs) {
		return Collections.emptyList();
	}

	@Override
	public void sendUsage(@NonNull CommandSenderWrapper sender) {
		sender.sendMessage(USAGE);
	}
}
