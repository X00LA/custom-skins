package de.janmm14.customskins.core.wrapper.config;

import de.janmm14.customskins.core.wrapper.Wrapper;

import lombok.NonNull;

public interface ConfigProvider extends Wrapper {

	@SuppressWarnings("NullableProblems")
	@NonNull
	ConfigWrapper getConfigWrapper();
}
