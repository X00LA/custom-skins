package de.janmm14.customskins.core.commands;

import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;
import de.janmm14.customskins.core.wrapper.PluginWrapper;

import lombok.NonNull;

public abstract class PlayerOnlyCmdPart extends CmdPart {

	public PlayerOnlyCmdPart(@NonNull PluginWrapper plugin, @NonNull String permission, @NonNull String... commandParts) {
		super(plugin, permission, commandParts);
	}

	@Override
	public final void onCommand(@NonNull CommandSenderWrapper sender, @NonNull String[] restArgs) {
		if (sender.isPlayer()) {
			onPlayerCommand(sender, restArgs);
		} else {
			sender.sendMessage("§4This command may only be used by players.");
		}
	}

	public abstract void onPlayerCommand(@NonNull CommandSenderWrapper p, @NonNull String[] restArgs);

}
