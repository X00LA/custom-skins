package de.janmm14.customskins.core.wrapper;

import lombok.NonNull;

public interface SchedulerWrapper extends Wrapper {

	void asyncNow(@NonNull Runnable task);
}
