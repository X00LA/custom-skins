package de.janmm14.customskins.core.data;

import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;
import javax.annotation.Nullable;

import de.janmm14.customskins.core.util.Util;
import de.janmm14.customskins.core.wrapper.PluginWrapper;
import de.janmm14.customskins.core.wrapper.config.ConfigWrapper;
import de.janmm14.customskins.core.wrapper.config.ConfigurationSectionWrapper;

import org.jetbrains.annotations.Contract;
import lombok.NonNull;

public class Data {

	@NonNull
	private final PluginWrapper plugin;
	@NonNull
	private ConfigWrapper cfg;

	@Nullable
	private HashSet<String> availableSkinNamesCache;
	@Nullable
	private HashSet<Account> accountCache;

	@Nullable
	private HashSet<Proxy> proxyCache;

	public Data(@NonNull PluginWrapper plugin) {
		this.plugin = plugin;
		cfg = plugin.getConfigWrapper();
		reloadConfig();
	}

	public long getAccountCooldown() {
		return cfg.getLong("mojangaccountcooldown");
	}

	public boolean isDebug() {
		return cfg.getBoolean("debug");
	}

	public void reloadConfig() {
		accountCache = null;
		availableSkinNamesCache = null;
		proxyCache = null;

		cfg = plugin.getConfigWrapper();
		cfg.reloadConfig();
		cfg.options().copyDefaults(true);

		cfg.addDefault("debug", false);
		cfg.addDefault("mojangaccountcooldown", 30);
		cfg.set("mojangaccountcooldowninfo", "In Seconds. 0 to disable. 30 seems to be a value working, however you can lower it and look whether its still working.");
		cfg.set("mojangaccounts.dummyaccount.uuid", "the uuid of dummyaccount");
		cfg.set("mojangaccounts.dummyaccount.email", "dummy@example.com");
		cfg.set("mojangaccounts.dummyaccount.password", "password");
		cfg.set("mojangaccounts.dummyaccount.info", Arrays.asList("You should not delete this, any accounts with the email dummy@example.com are ignored.", "NEVER OVERRIDE THE EXAMPLES! THEY ARE RESET EVERY PLUGIN / SERVER RELOAD!"));
		ConfigurationSectionWrapper proxyapis = cfg.getConfigurationSection("proxyapis");
		// backup old yasakvar config section if not empty
		if (proxyapis != null && proxyapis.isValid() && !proxyapis.getKeys(false).isEmpty()) {
			cfg.set("proxyapis-config-old-not-supported-anymore", proxyapis.getWrapped());
		}
		cfg.set("proxyapis.yasakvar.enabled", null);
		cfg.set("proxyapis.yasakvar.countries", null);
		cfg.set("proxyapis.yasakvar.info", null);
		cfg.set("proxyapis.yasakvar", null);
		cfg.set("proxyapis", null);
		cfg.set("proxies.configexample.host", "proxy.example.com");
		cfg.set("proxies.configexample.port", 8080);
		cfg.set("proxies.configexamplewithcredentials.host", "proxycredentials.example.com");
		cfg.set("proxies.configexamplewithcredentials.port", 39124);
		cfg.set("proxies.configexamplewithcredentials.username", "username");
		cfg.set("proxies.configexamplewithcredentials.password", "password");
		cfg.set("proxies.configexample.info", "Any proxy with any of the hosts of the examples are ignored. NEVER OVERRIDE THE EXAMPLES! THEY ARE RESET EVERY PLUGIN / SERVER RELOAD!");
		saveConfig();
	}

	public void saveConfig() {
		plugin.getConfigWrapper().saveConfig();
	}

	@NonNull
	public HashSet<Account> getAccounts() {
		if (accountCache == null) {
			HashSet<Account> set = new HashSet<>();
			for (String accName : cfg.getConfigurationSection("mojangaccounts").getKeys(false)) {
				String email = cfg.getString("mojangaccounts." + accName + ".email");
				if (!"dummy@example.com".equalsIgnoreCase(email)) {
					String uuid = cfg.getString("mojangaccounts." + accName + ".uuid");
					if (!uuid.contains("-")) {
						uuid = Util.insertDashesToUuidString(uuid);
					}
					String password = cfg.getString("mojangaccounts." + accName + ".password");
					set.add(new Account(accName, UUID.fromString(uuid), email, password));
				}
			}
			return set;
		} else {
			return accountCache;
		}
	}

	@NonNull
	public HashSet<String> getAllSkinNames() {
		if (availableSkinNamesCache == null) {
			HashSet<String> set = new HashSet<>();
			for (String id : cfg.getConfigurationSection("skins").getKeys(false)) {
				set.add(id);
			}
			return set;
		} else {
			return availableSkinNamesCache;
		}
	}

	@Nullable
	public Skin getCachedSkin(@NonNull String skinName) {
		skinName = skinName.toLowerCase();
		String data = cfg.getString("skins." + skinName + ".data");
		if (data == null || data.trim().isEmpty())
			return null;
		return new Skin(skinName, data, cfg.getString("skins." + skinName + ".signature"));
	}

	public void setCachedSkin(@NonNull Skin skin, @NonNull String source) {
		//skinCache.put(skin.getSkinName(), skin);
		cfg.set("skins." + skin.getSkinName() + ".data", skin.getData());
		cfg.set("skins." + skin.getSkinName() + ".signature", skin.getSignature());
		cfg.set("skins." + skin.getSkinName() + ".source", source);
		saveConfig();
	}

	@Contract("null,_ -> fail; !null,null -> _; !null,!null -> !null")
	public String getSkinSource(@NonNull Skin skin, @Nullable String unknownSource) {
		return cfg.getString("skins." + skin.getSkinName() + ".source", unknownSource);
	}

	public boolean deleteCachedSkin(@NonNull String skinName) {
		if (getCachedSkin(skinName) == null)
			return false;
		//skinCache.remove(skinName);
		cfg.set("skins." + skinName + ".data", null);
		cfg.set("skins." + skinName + ".signature", null);
		cfg.set("skins." + skinName + ".source", null);
		cfg.set("skins." + skinName, null);
		// TODO reset players using the deleted skin (can maybe be done on-the-fly, for example if the data is accessed
		saveConfig();
		return true;
	}

	@NonNull
	public HashSet<Proxy> getProxies() {
		if (proxyCache == null) {
			HashSet<Proxy> set = new HashSet<>();
			set.add(Proxy.NO_PROXY);
			for (String pid : cfg.getConfigurationSection("proxies").getKeys(false)) {
				String host = cfg.getString("proxies." + pid + "host");
				if (host == null || host.isEmpty() || host.equalsIgnoreCase("proxy.example.com") || host.equalsIgnoreCase("proxycredentials.example.com")) {
					continue;
				}
				String username = cfg.getString("proxies." + pid + "username");
				int port = cfg.getInt("proxies." + pid + "port");
				if (username == null || username.trim().isEmpty()) {
					set.add(Proxy.create(pid, host, port));
				} else {
					String password = cfg.getString("proxies." + pid + "password");
					set.add(Proxy.create(pid, host, port, username, password));
				}
			}
			proxyCache = set;
		}
		return proxyCache;
	}

	public String getSkinNameIdByUuid(@NonNull UUID uuid) {
		/*
		if (!uuidSkinIdCache.containsKey(uuid)) {
		String skinId = cfg.getString("usedskins." + uuid);
		uuidSkinIdCache.put(uuid, skinId);
		return skinId;
		} else {
			return uuidSkinIdCache.get(uuid);
		}
		*/
		return cfg.getString("usedskins." + uuid);
	}

	public void setUsedSkin(@NonNull UUID uuid, @NonNull String skinId) {
		cfg.set("usedskins." + uuid, skinId);
		//uuidSkinIdCache.put(uuid, skinId);
		saveConfig();
	}

	public void resetSkin(@NonNull UUID uuid) {
		cfg.set("usedskins." + uuid, null);
		//uuidSkinIdCache.remove(uuid);
		plugin.getSkinHandler().updatePlayerSkin(uuid);
		saveConfig();
	}
}
