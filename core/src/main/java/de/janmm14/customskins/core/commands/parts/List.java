package de.janmm14.customskins.core.commands.parts;

import java.util.Collections;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import de.janmm14.customskins.core.commands.CmdPart;
import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;
import de.janmm14.customskins.core.wrapper.PluginWrapper;

import com.google.common.collect.Lists;
import lombok.NonNull;

public class List extends CmdPart {

	public static final BaseComponent[] USAGE = new ComponentBuilder("/customskins list").color(ChatColor.DARK_GRAY).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/customskins list"))
		.append(" - ").color(ChatColor.DARK_GRAY)
		.append("lists all downloaded skins").color(ChatColor.DARK_GRAY).create();

	public List(PluginWrapper plugin) {
		super(plugin, "customskins.list", "list");
	}

	@Override
	public void onCommand(@NonNull CommandSenderWrapper cs, @NonNull String[] restArgs) {
		cs.sendMessage("§6All downloaded custom skins:"); //TODO pages?
		int i = 0;
		StringBuilder sb = new StringBuilder();
		for (String skinName : getPlugin().getData().getAllSkinNames()) {

			if (i != 0) {
				sb.append("§7, ");
			}
			sb.append(i % 2 == 1 ? "§6" : "§e").append(skinName);
			i++;
		}
		cs.sendMessage(sb.toString());
	}

	@NonNull
	@Override
	public java.util.List<String> onTabComplete(@NonNull CommandSenderWrapper sender, @NonNull String[] restArgs) {
		return Collections.emptyList();
	}

	@Override
	public void sendUsage(@NonNull CommandSenderWrapper sender) {
		sender.sendMessage(USAGE);
	}
}
