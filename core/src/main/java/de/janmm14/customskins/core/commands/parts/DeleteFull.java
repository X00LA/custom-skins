package de.janmm14.customskins.core.commands.parts;

import java.util.Collections;
import java.util.List;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import de.janmm14.customskins.core.commands.CmdPart;
import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;
import de.janmm14.customskins.core.wrapper.PluginWrapper;

import com.google.common.collect.Lists;
import lombok.NonNull;

public class DeleteFull extends CmdPart {

	public static final BaseComponent[] USAGE = new ComponentBuilder("/customskins deletefull <skinname>").strikethrough(true).color(ChatColor.DARK_GRAY).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/customskins deletefull "))
		.append(" - ").color(ChatColor.DARK_GRAY)
		.append("delets the skin with the provided skin name fully").color(ChatColor.DARK_GRAY).create();

	public DeleteFull(PluginWrapper plugin) {
		super(plugin, "customskins.deletefull", "deletefull");
	}

	@Override
	public void onCommand(@NonNull CommandSenderWrapper sender, @NonNull String[] restArgs) {
		sender.sendMessage("§cThis command is not available yet. Look for future updates of this plugin.");
		// TODO implement
	}

	@NonNull
	@Override
	public List<String> onTabComplete(@NonNull CommandSenderWrapper sender, @NonNull String[] restArgs) {
		return Collections.emptyList();
	}


	@Override
	public void sendUsage(@NonNull CommandSenderWrapper sender) {
		sender.sendMessage(USAGE);
	}
}
