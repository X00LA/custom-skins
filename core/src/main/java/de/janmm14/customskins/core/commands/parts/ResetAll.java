package de.janmm14.customskins.core.commands.parts;

import java.util.Collections;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;

import de.janmm14.customskins.core.commands.CmdPart;
import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;
import de.janmm14.customskins.core.wrapper.PluginWrapper;

import com.google.common.collect.Lists;
import lombok.NonNull;

public class ResetAll extends CmdPart {

	public static final BaseComponent[] USAGE = new ComponentBuilder("/customskins resetall").strikethrough(true).color(ChatColor.DARK_GRAY).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/customskins resetall"))
		.append(" - ").color(ChatColor.DARK_GRAY)
		.append("resets ").color(ChatColor.DARK_GRAY)
		.append("ALL").color(ChatColor.DARK_GRAY).bold(true).underlined(true).
			event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("WARNING!!!").color(ChatColor.DARK_RED).bold(true).underlined(true).create()))
		.append(" skins to the default minecraft.net ones", ComponentBuilder.FormatRetention.NONE).color(ChatColor.DARK_GRAY).create();

	public ResetAll(PluginWrapper plugin) {
		super(plugin, "customskins.reset", "reset");
	}

	@Override
	public void onCommand(@NonNull CommandSenderWrapper sender, @NonNull String[] restArgs) {
		sender.sendMessage("This command is not available yet. Look for future updates of this plugin or stop the server and delete the part in the config file.");
		// TODO implement
		// TODO double-check/ask if the player really wants it
	}

	@NonNull
	@Override
	public java.util.List<String> onTabComplete(@NonNull CommandSenderWrapper sender, @NonNull String[] restArgs) {
		return Collections.emptyList();
	}

	@Override
	public void sendUsage(@NonNull CommandSenderWrapper sender) {
		sender.sendMessage(USAGE);
	}
}
