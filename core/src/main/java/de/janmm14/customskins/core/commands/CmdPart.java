package de.janmm14.customskins.core.commands;

import java.util.List;

import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;
import de.janmm14.customskins.core.wrapper.PluginWrapper;

import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

@ToString
@EqualsAndHashCode(of = "permission")
public abstract class CmdPart {

	@NonNull
	private final PluginWrapper plugin;
	@NonNull
	private final String[] commandParts;
	@NonNull
	private final String permission;

	public CmdPart(@NonNull PluginWrapper plugin, @NonNull String permission, @NonNull String... commandParts) {
		this.plugin = plugin;
		this.commandParts = commandParts;
		this.permission = permission;
	}

	public abstract void onCommand(@NonNull CommandSenderWrapper sender, @NonNull String[] restArgs);

	@NonNull
	public abstract List<String> onTabComplete(@NonNull CommandSenderWrapper sender, @NonNull String[] restArgs);

	public abstract void sendUsage(@NonNull CommandSenderWrapper sender);

	@NonNull
	public final String[] getCommandParts() {
		return commandParts;
	}

	@NonNull
	public final String getPermission() {
		return permission;
	}

	@NonNull
	public PluginWrapper getPlugin() {
		return plugin;
	}
}
