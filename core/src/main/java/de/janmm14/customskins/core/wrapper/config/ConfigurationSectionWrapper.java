package de.janmm14.customskins.core.wrapper.config;

import java.util.Set;
import javax.annotation.Nullable;

import de.janmm14.customskins.core.wrapper.Wrapper;

import lombok.NonNull;

public interface ConfigurationSectionWrapper extends Wrapper {

	@Nullable
	String getString(@NonNull String path);

	@Nullable
	String getString(@NonNull String path, @Nullable String def);

	int getInt(@NonNull String path);

	long getLong(@NonNull String path);

	boolean getBoolean(@NonNull String path);

	void set(@NonNull String path, @Nullable Object value);

	void addDefault(@NonNull String path, @NonNull Object value);

	@NonNull
	Set<String> getKeys(boolean deep);

	@NonNull
	ConfigurationSectionWrapper getConfigurationSection(@NonNull String path);

	boolean isValid();
}
