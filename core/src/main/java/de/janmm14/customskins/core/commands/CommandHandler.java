package de.janmm14.customskins.core.commands;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import de.janmm14.customskins.core.commands.parts.DeleteFull;
import de.janmm14.customskins.core.commands.parts.Get;
import de.janmm14.customskins.core.commands.parts.Info;
import de.janmm14.customskins.core.commands.parts.Prepare;
import de.janmm14.customskins.core.commands.parts.Reload;
import de.janmm14.customskins.core.commands.parts.Reset;
import de.janmm14.customskins.core.commands.parts.ResetAll;
import de.janmm14.customskins.core.commands.parts.ResetMe;
import de.janmm14.customskins.core.commands.parts.Set;
import de.janmm14.customskins.core.commands.parts.SetMe;
import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;
import de.janmm14.customskins.core.wrapper.PluginWrapper;

import com.google.common.collect.Maps;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;

public class CommandHandler {

	@NonNull
	public static final BaseComponent[] USAGE_HEADER = new ComponentBuilder(">>> CustomSkins Help <<<").color(ChatColor.LIGHT_PURPLE).create();

	@Getter(AccessLevel.PROTECTED)
	@NonNull
	private final PluginWrapper plugin;
	@NonNull
	private final Map<String, CmdPart> cmdParts = Maps.newHashMapWithExpectedSize(16);

	public CommandHandler(@NonNull PluginWrapper plugin) {
		this.plugin = plugin;

		registerCmdPart(new Reload(plugin));
		registerCmdPart(new DeleteFull(plugin));
		registerCmdPart(new Info(plugin));
		registerCmdPart(new Prepare(plugin));
		registerCmdPart(new Reload(plugin));
		registerCmdPart(new Reset(plugin));
		registerCmdPart(new ResetAll(plugin));
		registerCmdPart(new ResetMe(plugin));
		registerCmdPart(new Set(plugin));
		registerCmdPart(new SetMe(plugin));
		registerCmdPart(new Get(plugin));
	}

	private void registerCmdPart(@NonNull CmdPart cmdPart) {
		for (String part : cmdPart.getCommandParts()) {
			cmdParts.put(part.trim().toLowerCase(), cmdPart);
		}
	}

	public boolean onCommand(CommandSenderWrapper cs, String baseCmd, String alias, String[] args) {
		if (!baseCmd.equalsIgnoreCase("customskins")) {
			cs.sendMessage("§4Unknown command §c" + alias + " §4attached to CustomSkins!");
			plugin.getLogger().warning(cs + " used unknown command " + baseCmd + " which was attached to CustomSkins! Used alias: " + alias);
			return true;
		}
		if (args.length > 0) {
			String args0 = args[0].trim().toLowerCase(Locale.ENGLISH);
			if (cmdParts.containsKey(args0)) {
				CmdPart cmdPart = cmdParts.get(args0.trim().toLowerCase());
				if (cs.hasPermission(cmdPart.getPermission())) {
					cmdPart.onCommand(cs, Arrays.copyOfRange(args, 1, args.length));
					return true;
				} else {
					cs.sendMessage("§4You are not allowed to use this command!");
					return true;
				}
			} else {
				cs.sendMessage("§4Subcommand §6" + args0 + " §4not found!");
			}
		}
		cs.sendMessage(USAGE_HEADER);
		boolean sent = false;
		for (CmdPart cmdPart : cmdParts.values()) {
			if (!cs.hasPermission(cmdPart.getPermission())) {
				continue;
			}
			sent = true;
			if (!cs.isPlayer() && cmdPart instanceof PlayerOnlyCmdPart) {
				cs.sendMessage("The following command may only be used by players:");
			}
			cmdPart.sendUsage(cs);
		}
		if (!sent) {
			cs.sendMessage("§cYou do not have permission to any command of customskins.");
		}
		return true;
	}

	public List<String> onTabComplete(CommandSenderWrapper sender, String baseCmd, String alias, String[] args) {
		if (!baseCmd.equalsIgnoreCase("customskins")) {
			return null;
		}
		// TODO implement
		if (plugin.getData().isDebug()) {
			sender.sendMessage(Arrays.toString(args));
		}
		return null;
	}
}
