package de.janmm14.customskins.core.commands.parts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import de.janmm14.customskins.core.commands.CmdPart;
import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;
import de.janmm14.customskins.core.wrapper.PluginWrapper;

import com.google.common.collect.Lists;
import lombok.NonNull;

public class Info extends CmdPart {

	public static final BaseComponent[] USAGE = new ComponentBuilder("/customskins info <player>").strikethrough(true).color(ChatColor.DARK_GRAY).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/customskins info "))
		.append(" - ").color(ChatColor.DARK_GRAY)
		.append("shows informations about the current skin of the given player").color(ChatColor.DARK_GRAY).create();

	public Info(PluginWrapper plugin) {
		super(plugin, "customskins.info", "info");
	}

	@Override
	public void onCommand(@NonNull CommandSenderWrapper cs, @NonNull String[] restArgs) {
		cs.sendMessage("§cThis command is not available yet. Look for future updates of this plugin.");
		// TODO implement
	}

	@NonNull
	@Override
	public List<String> onTabComplete(@NonNull CommandSenderWrapper cs, @NonNull String[] args) {
		if (args.length == 0)
			return Collections.emptyList();

		String s = args[0].toLowerCase();
		List<String> l = new ArrayList<>();
		for (CommandSenderWrapper p : getPlugin().getOnlinePlayers()) {
			String name = p.getName();
			if (name.toLowerCase().startsWith(s)) {
				l.add(name);
			}
		}
		return l;
	}

	@Override
	public void sendUsage(@NonNull CommandSenderWrapper sender) {
		sender.sendMessage(USAGE);
	}
}
