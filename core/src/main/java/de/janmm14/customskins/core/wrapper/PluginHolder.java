package de.janmm14.customskins.core.wrapper;

import lombok.NonNull;

public final class PluginHolder {

	private static PluginWrapper plugin;

	private PluginHolder() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @return PluignWrapper or null, if not set
	 * @deprecated Static instance-getting methods are not encouraged to use
	 */
	@Deprecated
	public static PluginWrapper getPlugin() {
		return plugin;
	}

	public static void setPlugin(@NonNull PluginWrapper plugin) {
		PluginHolder.plugin = plugin;
	}
}
