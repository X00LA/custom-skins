package de.janmm14.customskins.core.commands.parts;

import java.util.Collections;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;

import de.janmm14.customskins.core.commands.CmdPart;
import de.janmm14.customskins.core.wrapper.CommandSenderWrapper;
import de.janmm14.customskins.core.wrapper.PluginWrapper;

import lombok.NonNull;

public class Reset extends CmdPart {

	public static final BaseComponent[] USAGE = new ComponentBuilder("/customskins reset <name>").strikethrough(true).color(ChatColor.DARK_GRAY).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/customskins reset "))
		.append(" - ").color(ChatColor.DARK_GRAY)
		.append("resets the skin of the given player to the default minecraft.net one").color(ChatColor.DARK_GRAY).create();

	public Reset(PluginWrapper plugin) {
		super(plugin, "customskins.reset", "reset");
	}

	@Override
	public void onCommand(@NonNull CommandSenderWrapper sender, @NonNull String[] restArgs) {
		sender.sendMessage("§cThis command is not available yet. Look for future updates of this plugin.");
		// TODO implement
	}

	@NonNull
	@Override
	public java.util.List<String> onTabComplete(@NonNull CommandSenderWrapper sender, @NonNull String[] restArgs) {
		// TODO implement
		return Collections.emptyList();
	}

	@Override
	public void sendUsage(@NonNull CommandSenderWrapper sender) {
		sender.sendMessage(USAGE);
	}
}
